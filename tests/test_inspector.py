""" ae.inspect unit tests """
import pytest
import os
import textwrap
from types import ModuleType
from typing import cast

from ae.base import DATE_ISO, PY_EXT, TESTS_FOLDER, UNSET, write_file
from ae.inspector import (exec_with_return, full_stack_trace, module_attr, module_file_path, module_name,
                          stack_frames, stack_var, stack_vars, try_call, try_eval, try_exec)

import datetime as test_dt


module_var = 'module_var_val'   # used for stack_var()/try_exec() tests


class TestHelpers:
    def test_exec_with_return_basics(self):
        assert exec_with_return("") is UNSET
        assert exec_with_return('1 + 2') == 3
        assert exec_with_return('a = 1 + 2; a') == 3
        assert exec_with_return('a = 1 + 2; a + 3') == 6
        assert exec_with_return('\na = 1 + 2\na\n') == 3

    def test_exec_with_return_syntax_err(self):
        syn_err = 'if :'
        with pytest.raises(SyntaxError):
            exec_with_return(syn_err)
        assert exec_with_return(syn_err, ignored_exceptions=(SyntaxError, )) is UNSET

    def test_exec_with_return_code_block(self):
        if_stmt = """
                        if 1 + 2 == 3:
                            a = 6
                        else:
                            a = 9
                        a
        """
        with pytest.raises(IndentationError):
            exec_with_return(if_stmt)
        if_stmt = textwrap.dedent(if_stmt)
        assert exec_with_return(if_stmt) == 6

    def test_exec_with_return_vars(self):
        assert exec_with_return('a = b + 6; a', glo_vars=dict(b=3)) == 9
        assert exec_with_return('a = b + 6; a', loc_vars=dict(b=3)) == 9
        assert exec_with_return('a = b + 6; a', glo_vars=dict(b=69), loc_vars=dict(b=3)) == 9

        loc_vars = dict(b=3)
        assert exec_with_return('a = b + 6', glo_vars=dict(b=69), loc_vars=loc_vars) is UNSET
        assert loc_vars.get('a') == 9

    def test_exec_with_return_add_global_vars(self):
        assert exec_with_return('datetime.date(2100, 12, 21).year') == 2100

        glo_vars = dict(_add_base_globals="")
        assert exec_with_return('datetime.date(2100, 12, 21).year', glo_vars=glo_vars) == 2100

        with pytest.raises(NameError):
            assert exec_with_return('datetime.date(2100, 12, 21).year', glo_vars={}) == 2100

    def test_full_stack_trace(self):
        try:
            raise ValueError
        except ValueError as ex:
            # print(full_stack_trace(ex))
            assert full_stack_trace(ex)

    def test_module_attr_callable_with_args(self):
        namespace = TESTS_FOLDER
        mod_name = 'test_module_name'
        att_name = 'test_module_func'
        module_file = os.path.join(namespace, mod_name + PY_EXT)
        try:
            write_file(module_file, f"def {att_name}(*args, **kwargs):\n    return args, kwargs\n")
            args = (1, '2')
            kwargs = dict(kwarg1=1, kwarg2='2')

            ret = module_attr(namespace + '.' + mod_name, attr_name=att_name)
            assert ret
            assert callable(type(ret))

            call_ret = try_call(ret, *args, **kwargs)
            assert call_ret
            assert call_ret[0] == args
            assert call_ret[1] == kwargs

        finally:
            if os.path.exists(module_file):
                os.remove(module_file)

        # test already imported module
        callee = module_attr('textwrap', attr_name='indent')
        assert callable(callee)
        assert callee is textwrap.indent

    def test_module_attr_callable_wrong_args(self):
        namespace = TESTS_FOLDER
        mod_name = 'test_module_name'
        att_name = 'test_module_func'
        module_file = os.path.join(namespace, mod_name + PY_EXT)
        try:
            write_file(module_file, f"def {att_name}(arg1, args2, kwarg1='default'):\n    return arg1, arg2, kwarg1\n")

            callee = module_attr(namespace + '.' + mod_name, attr_name=att_name)
            assert callable(callee)

            args = (1, '2')
            kwargs = dict(kwarg1=1, kwarg2='2')
            with pytest.raises(TypeError):
                try_call(callee, *args, **kwargs)
            ret = try_call(callee, *args, ignored_exceptions=(TypeError, ), **kwargs)
            assert ret is UNSET

        finally:
            if os.path.exists(module_file):
                os.remove(module_file)

    def test_module_attr_imported(self):
        """ test with module w/ and w/o namespace. """
        assert isinstance(module_attr('os'), ModuleType)
        assert isinstance(module_attr('textwrap'), ModuleType)
        assert isinstance(module_attr('ae.base'), ModuleType)
        assert isinstance(module_attr('ae.inspector'), ModuleType)

    def test_module_attr_module_ref(self):
        namespace = TESTS_FOLDER
        mod_name = 'test_module_name'
        module_file = os.path.join(namespace, mod_name + PY_EXT)
        cur_dir = os.getcwd()
        try:
            write_file(module_file, "# empty module")

            ret = module_attr(namespace + '.' + mod_name)
            assert isinstance(ret, ModuleType)

            os.chdir(namespace)

            ret = module_attr(mod_name)
            assert isinstance(ret, ModuleType)

        finally:
            os.chdir(cur_dir)
            if os.path.exists(module_file):
                os.remove(module_file)

    def test_module_attr_not_exists_attr(self):
        """ first test with non-existing module, second test with non-existing function. """
        namespace = TESTS_FOLDER
        mod_name = 'test_module_name'
        att_name = 'test_module_func'
        module_file = os.path.join(namespace, mod_name + PY_EXT)
        cur_dir = os.getcwd()
        try:
            write_file(module_file, f"""def {att_name}(*args, **kwargs):\n    pass\n""")

            ret = module_attr(namespace + '.' + mod_name, attr_name="not_existing_func_or_attr")
            assert ret is UNSET

            ret = module_attr(namespace + '.' + mod_name, attr_name=att_name)
            assert callable(ret)

            os.chdir(namespace)

            ret = module_attr(mod_name)
            assert ret
            assert type(ret) is ModuleType

        finally:
            os.chdir(cur_dir)
            if os.path.exists(module_file):
                os.remove(module_file)

    def test_module_attr_not_exists_module(self):
        """ first test with non-existing module, second test with non-existing function. """
        mod_name = 'non_existing_test_module_name'
        att_name = 'non_existing_test_module_func'
        assert module_attr(mod_name, attr_name=att_name) is None

    def test_module_file_path(self):
        assert module_file_path()
        assert module_file_path(lambda: 0)

    def test_module_name(self):
        assert module_name() == 'test_inspector'
        assert module_name('') == 'test_inspector'
        assert module_name(cast(str, None)) == 'test_inspector'
        assert module_name('_invalid_module_name') == 'test_inspector'
        assert module_name('ae.inspector') == 'test_inspector'
        assert module_name(depth=-30) == 'test_inspector'
        assert module_name(depth=-2) == 'test_inspector'
        assert module_name(depth=-1) == 'test_inspector'
        # assert module_name(depth=0) == 'test_inspector'   # depth=0 is default value
        # assert module_name(depth=1) == '_pytest.python'

        assert module_name(__name__, depth=-30) == 'ae.inspector'
        assert module_name(__name__, depth=-2) == 'ae.inspector'
        assert module_name(__name__, depth=-1) == 'ae.inspector'

        # assert module_name(__name__) == '_pytest.python'                  # depth=0 is the default
        # assert module_name('test_inspector') == '_pytest.python'
        # assert module_name(__name__, depth=1) == '_pytest.python'

    def test_stack_frames(self):
        for frame in stack_frames():
            assert frame
            assert getattr(frame, 'f_globals')
            # if pytest runs from terminal then f_locals is missing in the highest frame:
            # assert getattr(frame, 'f_locals')

    def test_stack_var_module(self):
        assert module_var
        assert stack_var('module_var', depth=-1) == 'module_var_val'
        assert stack_var('module_var', depth=0) == 'module_var_val'
        assert stack_var('module_var', scope='globals', depth=0) == 'module_var_val'
        assert stack_var('module_var', 'ae.inspector', depth=0) == 'module_var_val'

        assert stack_var('module_var') is UNSET      # depth==1 (def)
        assert stack_var('module_var', depth=2) is UNSET
        assert stack_var('module_var', scope='locals', depth=0) is UNSET
        assert stack_var('module_var', scope='locals') is UNSET
        assert stack_var('module_var', 'test_inspector') is UNSET
        assert stack_var('module_var', 'ae.inspector', 'test_inspector') is UNSET

    def test_stack_var_func(self):
        _func_var = 'func_var_val'

        assert stack_var('_func_var', 'ae.inspector', scope='locals', depth=0) == 'func_var_val'
        assert stack_var('_func_var', depth=0) == 'func_var_val'
        assert stack_var('_func_var', scope='locals', depth=0) == 'func_var_val'

        # assert stack_var('_func_var', scope='locals', depth=1) is UNSET
        assert stack_var('_func_var') is UNSET
        assert stack_var('_func_var', scope='globals', depth=0) is UNSET
        assert stack_var('_func_var', 'test_inspector', scope='locals') is UNSET
        assert stack_var('_func_var', 'ae.inspector', 'test_inspector', scope='locals') is UNSET
        assert stack_var('_func_var', scope='locals', depth=3) is UNSET

    def test_stack_var_inner_func(self):
        def _inner_func():
            _inner_var = 'inner_var_val'
            assert stack_var('_inner_var', depth=-1) == 'inner_var_val'
            assert stack_var('_inner_var', depth=0) == 'inner_var_val'
            assert stack_var('_inner_var', scope='locals', depth=0) == 'inner_var_val'
            assert stack_var('_inner_var', 'ae.inspector', scope='locals', depth=0) == 'inner_var_val'
            assert stack_var('_inner_var', 'ae.inspector', 'xxx yyy', scope='locals', depth=0) == 'inner_var_val'

            assert stack_var('_inner_var') is UNSET     # depth==1 (def)
            assert stack_var('_inner_var', depth=2) is UNSET
            assert stack_var('_inner_var', scope='globals', depth=0) is UNSET
            assert stack_var('_inner_var', 'test_inspector', scope='locals', depth=0) is UNSET

            assert stack_var('_outer_var') == 'outer_var_val'
            assert stack_var('_outer_var', depth=0) == 'outer_var_val'
            assert stack_var('_outer_var', 'ae.inspector', scope='locals') == 'outer_var_val'
            assert stack_var('_outer_var', scope='locals') == 'outer_var_val'
            assert stack_var('_outer_var', scope='locals', depth=0) == 'outer_var_val'

            assert stack_var('_outer_var', scope='locals', depth=2) is UNSET
            assert stack_var('_outer_var', 'test_inspector', scope='locals') is UNSET
            assert stack_var('_outer_var', 'ae.inspector', 'test_inspector', scope='locals') is UNSET

            assert stack_var('module_var') == 'module_var_val'
            assert stack_var('module_var', scope='globals') == 'module_var_val'

            assert stack_var('module_var', depth=2) is UNSET
            assert stack_var('module_var', scope='locals') is UNSET
            assert stack_var('module_var', 'test_inspector') is UNSET
            assert stack_var('module_var', 'ae.inspector', 'test_inspector') is UNSET

        _outer_var = 'outer_var_val'
        _inner_func()

        assert stack_var('_outer_var', depth=0) == 'outer_var_val'
        assert stack_var('_outer_var', 'ae.inspector', scope='locals', depth=0) == 'outer_var_val'
        assert stack_var('_outer_var', scope='locals', depth=0) == 'outer_var_val'

        assert stack_var('_outer_var') is UNSET
        assert stack_var('_outer_var', scope='locals') is UNSET
        assert stack_var('_outer_var', scope='locals', depth=2) is UNSET
        assert stack_var('_outer_var', 'test_inspector') is UNSET

        assert stack_var('module_var', depth=0) == 'module_var_val'
        assert stack_var('module_var', depth=0, scope='globals') == 'module_var_val'

        assert stack_var('module_var') is UNSET
        assert stack_var('module_var', depth=2) is UNSET
        assert stack_var('module_var', depth=3) is UNSET
        assert stack_var('module_var', scope='locals', depth=0) is UNSET
        assert stack_var('module_var', 'test_inspector') is UNSET
        assert stack_var('module_var', 'ae.inspector', 'test_inspector') is UNSET

    def test_stack_vars(self):
        local_var = "loc_var_val"
        glo, loc, deep = stack_vars(min_depth=0, max_depth=1)
        assert deep == 1
        assert 'local_var' in loc
        assert loc['local_var'] == local_var

        glo, loc, deep = stack_vars(max_depth=3)
        assert deep == 3

        glo, loc, deep = stack_vars(min_depth=0, find_name='module_var')    # min_depth needed for this stack frame
        assert glo.get('module_var') == 'module_var_val'

        glo, loc, deep = stack_vars(find_name='module_var')                 # min_depth default == 1
        assert glo.get('module_var') is None

        glo, loc, deep = stack_vars(min_depth=2, find_name='module_var')    # min_depth needed for this stack frame
        assert glo.get('module_var') is None

    def test_try_call(self):
        assert try_call(str, 123) == "123"
        assert try_call(bytes, '123', encoding='ascii') == b"123"
        assert try_call(int, '123') == 123

        call_arg = "no-number"
        with pytest.raises(ValueError):
            assert try_call(int, call_arg)
        assert try_call(int, call_arg, ignored_exceptions=(ValueError, )) is UNSET

    def test_try_eval_basics(self):
        assert try_eval("str(123)") == "123"
        assert try_eval("str(bytes(b'123'), encoding='ascii')") == "123"
        assert try_eval("int('123')") == 123

    def test_try_eval_syntax_err(self):
        eval_str = "int('no-number')"
        with pytest.raises(ValueError):
            assert try_eval(eval_str)
        assert try_eval(eval_str, ignored_exceptions=(ValueError, )) is UNSET
        with pytest.raises(TypeError):      # list with ignored exceptions is not accepted
            assert try_eval(eval_str, ignored_exceptions=cast(tuple, [ValueError, ])) is UNSET

    def test_try_eval_vars(self):
        assert try_eval('b + 6', glo_vars=dict(b=3)) == 9
        assert try_eval('b + 6', loc_vars=dict(b=3)) == 9
        assert try_eval('b + 6', glo_vars=dict(b=33), loc_vars=dict(b=3)) == 9

    def test_try_eval_add_global_vars(self):
        assert try_eval('datetime.date(2100, 12, 21).year') == 2100

        glo_vars = dict(_add_base_globals="")
        assert try_eval('datetime.date(2100, 12, 21).year', glo_vars=glo_vars) == 2100

        with pytest.raises(NameError):
            assert try_eval('datetime.date(2100, 12, 21).year', glo_vars={}) == 2100

    def test_try_exec(self):
        assert try_exec('a = 1 + 2; a') == 3
        assert try_exec('a = 1 + 2; a + 3') == 6
        assert try_exec('a = b + 6; a', glo_vars=dict(b=3)) == 9
        assert try_exec('a = b + 6; a', loc_vars=dict(b=3)) == 9
        assert try_exec('a = b + 6; a', glo_vars=dict(b=69), loc_vars=dict(b=3)) == 9

        code_block = "a=1+2; module_var"
        with pytest.raises(NameError):
            assert try_exec(code_block) == module_var
        assert try_exec(code_block, ignored_exceptions=(NameError, )) is UNSET
        assert try_exec(code_block, glo_vars=globals()) == module_var

        # check ae.core datetime/DATE_ISO context (globals)
        dt_val = test_dt.datetime.now()
        dt_str = test_dt.datetime.strftime(dt_val, DATE_ISO)
        assert try_exec("dt = _; datetime.datetime.strftime(dt, DATE_ISO)", loc_vars={'_': dt_val}) == dt_str
