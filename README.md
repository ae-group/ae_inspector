<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.88 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.10 -->
# inspector 0.3.13

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_inspector/develop?logo=python)](
    https://gitlab.com/ae-group/ae_inspector)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_inspector/release0.3.12?logo=python)](
    https://gitlab.com/ae-group/ae_inspector/-/tree/release0.3.12)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_inspector)](
    https://pypi.org/project/ae-inspector/#history)

>ae namespace module portion inspector: inspection and debugging helper functions.

[![Coverage](https://ae-group.gitlab.io/ae_inspector/coverage.svg)](
    https://ae-group.gitlab.io/ae_inspector/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_inspector/mypy.svg)](
    https://ae-group.gitlab.io/ae_inspector/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_inspector/pylint.svg)](
    https://ae-group.gitlab.io/ae_inspector/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_inspector)](
    https://gitlab.com/ae-group/ae_inspector/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_inspector)](
    https://gitlab.com/ae-group/ae_inspector/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_inspector)](
    https://gitlab.com/ae-group/ae_inspector/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_inspector)](
    https://pypi.org/project/ae-inspector/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_inspector)](
    https://gitlab.com/ae-group/ae_inspector/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_inspector)](
    https://libraries.io/pypi/ae-inspector)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_inspector)](
    https://pypi.org/project/ae-inspector/#files)


## installation


execute the following command to install the
ae.inspector module
in the currently active virtual environment:
 
```shell script
pip install ae-inspector
```

if you want to contribute to this portion then first fork
[the ae_inspector repository at GitLab](
https://gitlab.com/ae-group/ae_inspector "ae.inspector code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_inspector):

```shell script
pip install -e .[dev]
```

the last command will install this module portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_inspector/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.inspector.html
"ae_inspector documentation").
